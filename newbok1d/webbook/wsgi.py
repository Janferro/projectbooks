"""
WSGI config for webbook project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os
#from whitenoise import WhiteNoise
from django.core.wsgi import get_wsgi_application
#from newbok1d import MyWSGIApp
#from whitenoise.django import DjangoWhiteNoise
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'webbook.settings')

application = get_wsgi_application()
#application = DjangoWhiteNoise(application)



#application = MyWSGIApp()
#application = WhiteNoise(application, root='/newbok1d/bokapp/static/bokapp/')
#application.add_files('/path/to/more/static/files', prefix='more-files/')
