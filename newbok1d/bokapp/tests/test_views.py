
import unittest
from django.test import TestCase, SimpleTestCase
from django.test.client import Client

from django.urls import reverse

client = Client()

# views

class BookfilterTest(TestCase):

    def test_bookfilter_status_code(self):
        response = self.client.get('/bokapp/book_filter/')
        self.assertEqual(response.status_code, 200)

    def test_bookfilter_url_name(self):
        response = self.client.get(reverse('bokapp:book_filter'))
        self.assertEquals(response.status_code, 200)

    def test_bookfilter_correct_template(self):
        response = self.client.get(reverse('bokapp:book_filter'))
        self.assertTemplateUsed(response, 'bokapp/books/book_filter.html')

    def test_bookfilter_contains_correct_html(self):
        response = self.client.get('/bokapp/book_filter/')
        self.assertContains(response, '<h4 style="margin-top: 0; color: orange;">Filtering books</h4>')


class BooksearchTest(TestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()

    def test_booksearch_status_code(self):
        response = self.client.get('/bokapp/book_search/')
        self.assertEqual(response.status_code, 200)

    def test_booksearch_url_name(self):
        response = self.client.get(reverse('bokapp:book_search'))
        self.assertEqual(response.status_code, 200)

    def test_booksearch_correct_template(self):
        response = self.client.get(reverse('bokapp:book_search'))
        self.assertTemplateUsed(response, 'bokapp/books/book_search.html')



if __name__ == '__main__':
    unittest.main()
    
