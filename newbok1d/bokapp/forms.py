from django import forms
import requests
import json
from .models import Books

class BookUpdateForm(forms.Form):

    title = forms.CharField(max_length=250)
    authors = forms.CharField(max_length=100)
    publisheddate = forms.IntegerField(min_value=1500, max_value=2030)
    isbn_number = forms.IntegerField(min_value=9780000000000, max_value=9799999999999)
    pagecount = forms.IntegerField(min_value=1, max_value=10000)
    language = forms.CharField(max_length=2)
    imagelinks = forms.URLField(max_length=200)

    class Meta:
        model = Books
        fields = '__all__'


class BookSearchForm(forms.Form):

    search_word = forms.CharField(max_length=50, required=False)
    title = forms.CharField(max_length=250, required=False)
    authors = forms.CharField(max_length=100, required=False)
    isbn_number = forms.IntegerField(required=False)

    def search(self):

        result = {}
        search_word = self.cleaned_data['search_word']
        title = self.cleaned_data['title']
        authors = self.cleaned_data['authors']
        isbn_number = self.cleaned_data['isbn_number']

        if search_word:
            s_word = search_word
        else:
            s_word = ""

        if title:
            titl_e = "+intitle:{}".format(title)
        else:
            titl_e = ""

        if authors:
            author_s = "+inauthor:{}".format(authors)
        else:
            author_s = ""

        if isbn_number:
            isb_n = "+isbn:{}".format(isbn_number)
        else:
            isb_n = ""

        url = "https://www.googleapis.com/books/v1/volumes?q={}".format(s_word) + author_s + titl_e + isb_n

        response = requests.get(url)

        if response.status_code == 200:  # SUCCESS
            result = json.loads(response.text)
        elif response.status_code > 200:
            result['message'] = False
        return result

