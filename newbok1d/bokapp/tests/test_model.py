from django.test import TestCase
from newbok1d.bokapp.models import Books
import unittest

class BooksTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        Books.objects.create(
                             title='Pan Tadeusz',
                             authors='A. Mickiewicz',
                             publisheddate='1834',
                             pagecount='300',
                             isbn_number='9788392789109',
                             imagelinks='https://pl.wikipedia.org/wiki/Pan_Tadeusz#/media/Plik:Pan_Tadeusz_1834.jpeg',
                             language='pl'
                             )


    def test_text_content(self):
        book = Books.objects.get(id=1)
        expected_object_name = f'{book.title}'
        self.assertEquals(expected_object_name, 'Pan Tadeusz')

    def test_title_label(self):
        book = Books.objects.get(id=1)
        field_label = book._meta.get_field('title').verbose_name
        self.assertEquals(field_label, 'title')


    def test_authors_label(self):
        book = Books.objects.get(id=1)
        field_label = book._meta.get_field('authors').verbose_name
        self.assertEquals(field_label, 'authors')


    def test_publishedDate_label(self):
        book = Books.objects.get(id=1)
        field_label = book._meta.get_field('publisheddate').verbose_name
        self.assertEquals(field_label, 'publisheddate')


    def test_ISBN_number_label(self):
        book = Books.objects.get(id=1)
        field_label = book._meta.get_field('isbn_number').verbose_name
        self.assertEquals(field_label, 'isbn number')


    def test_language_label(self):
        book = Books.objects.get(id=1)
        field_label = book._meta.get_field('language').verbose_name
        self.assertEquals(field_label, 'language')


    def test_imageLinks_label(self):
        book = Books.objects.get(id=1)
        field_label = book._meta.get_field('imagelinks').verbose_name
        self.assertEquals(field_label, 'imagelinks')


    def test_title_max_length(self):
        book = Books.objects.get(id=1)
        max_length = book._meta.get_field('title').max_length
        self.assertEquals(max_length, 250)

    def test_authors_max_length(self):
        book = Books.objects.get(id=1)
        max_length = book._meta.get_field('authors').max_length
        self.assertEquals(max_length, 100)

    def test_ISBN_number_max_length(self):
        book = Books.objects.get(id=1)
        max_length = book._meta.get_field('isbn_number').max_length
        self.assertEquals(max_length, 13)

    def test_language_max_length(self):
        book = Books.objects.get(id=1)
        max_length = book._meta.get_field('language').max_length
        self.assertEquals(max_length, 2)

    def test_imageLinks_max_length(self):
        book = Books.objects.get(id=1)
        max_length = book._meta.get_field('imagelinks').max_length
        self.assertEquals(max_length, 200)

    def create_books(self, title='Pan Tadeusz', authors='A. Mickiewicz', publisheddate='1834', pagecount='300',
                     isbn_number='9788392789109',
                     imagelinks='https://pl.wikipedia.org/wiki/Pan_Tadeusz#/media/Plik:Pan_Tadeusz_1834.jpeg',
                     language='pl'):
        return Books.objects.create(title=title, authors=authors, publisheddate=publisheddate, isbn_number=isbn_number,
                                    pagecount=pagecount, imagelinks=imagelinks, language=language)

    def test_books_creation(self):
        a = self.create_books()
        self.assertTrue(isinstance(a, Books))
        self.assertEqual(a.__str__(), a.title)



if __name__ == '__main__':
    unittest.main()