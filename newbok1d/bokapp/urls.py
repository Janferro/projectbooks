from django.urls import path
from . import views
from .views import *

app_name = 'bokapp'
urlpatterns = [
    # post views
    path('bokapp/book_search/', views.booksearch, name='book_search'),
    path('list/', IndexView.as_view(), name="book_list"),
    path('bokapp/book_filter/', views.bookfilter, name='book_filter'),
    path('add/', BooksCreateView.as_view(), name="books-add"),
    path('update/<int:pk>', BooksUpdateView.as_view(), name="books-update"),
    path('delete/<int:pk>/', BooksDeleteView.as_view(), name='delete'),

]