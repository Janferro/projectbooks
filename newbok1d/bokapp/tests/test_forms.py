from django.test import TestCase
from newbok1d.bokapp.forms import BookUpdateForm, BookSearchForm
from django.http import HttpRequest
from newbok1d.bokapp.models import Books

class BookUpdateFormTest(TestCase):

    def test_book_form_title_field_label(self):
        form = BookUpdateForm()
        self.assertTrue(form.fields['title'].label is None)

    def test_book_form_authors_field_label(self):
        form = BookUpdateForm()
        self.assertTrue(form.fields['authors'].label is None)

    def test_book_form_publisheddate_field_label(self):
        form = BookUpdateForm()
        self.assertTrue(form.fields['publisheddate'].label is None)

    def test_book_form_isbn_number_field_label(self):
        form = BookUpdateForm()
        self.assertTrue(form.fields['isbn_number'].label is None)

    def test_book_form_language_field_label(self):
        form = BookUpdateForm()
        self.assertTrue(form.fields['language'].label is None)

    def test_book_form_imagelinks_field_label(self):
        form = BookUpdateForm()
        self.assertTrue(form.fields['imagelinks'].label is None)


