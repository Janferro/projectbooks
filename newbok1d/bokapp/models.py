from django.db import models
from django.urls import reverse_lazy, reverse

class Books(models.Model):


    title = models.CharField(max_length=250)
    authors = models.CharField(max_length=100)
    publisheddate = models.PositiveSmallIntegerField()
    isbn_number = models.CharField(max_length=13)
    pagecount = models.PositiveSmallIntegerField()
    language = models.CharField(max_length=2)
    imagelinks = models.URLField(max_length=200)



    def get_absolute_url(self):
        return reverse('list-view', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title

