from django.shortcuts import render, get_object_or_404, redirect
from .models import Books
from .filters import BooksFilter
from .forms import BookSearchForm, BookUpdateForm
from django.contrib import messages
from django.views.generic import ListView
from django.views.generic import UpdateView, CreateView, DeleteView
from django.urls import reverse_lazy


class BooksCreateView(CreateView):
    model = Books
    fields = "__all__"
    template_name = 'bokapp/books/books_add.html'
    success_url = reverse_lazy('bokapp:book_list')

    def form_valid(self, form):

        if form.cleaned_data['title'].isalpha() == "":
            form.add_error('title', 'this field must be filled')
            return self.form_invalid(form)
        if form.cleaned_data['authors'].isalpha() == "":
            form.add_error('authors', 'this field must be filled')
            return self.form_invalid(form)
        if form.cleaned_data['publisheddate'] < 1500 or form.cleaned_data['publisheddate'] > 2030:
            form.add_error('publisheddate', 'this field: min_value=1500, max_value=2030')
            return self.form_invalid(form)
        if form.cleaned_data['pagecount'] < 1 or form.cleaned_data['pagecount'] > 10000:
            form.add_error('pagecount', 'this field: min_value=1, max_value=10000')
        if len(str(form.cleaned_data['isbn_number'])) != 13 or str(form.cleaned_data['isbn_number']).isdigit() is False:
            form.add_error('isbn_number', 'in this field must be only 13 numbers')
            return self.form_invalid(form)
        if form.cleaned_data['language'].isalpha() is False:
            form.add_error('language', 'in this field must be only 2 letters')
            return self.form_invalid(form)
        return super().form_valid(form)


class BooksUpdateView(UpdateView):
    model = Books
    fields = "__all__"
    template_name = 'bokapp/books/books_update.html'
    success_url = reverse_lazy('bokapp:book_list')

    def form_valid(self, form):
        if len(str(form.cleaned_data['isbn_number'])) != 13 or str(form.cleaned_data['isbn_number']).isdigit() is False:
            form.add_error('isbn_number', 'in this feiel must be only 13 numbers')
            return self.form_invalid(form)
        if form.cleaned_data['language'].isalpha() is False:
            form.add_error('language', 'in this feield must be only 2 letters')
            return self.form_invalid(form)
        return super().form_valid(form)


class IndexView(ListView):
    model = Books
    queryset = Books.objects.all()
    template_name = 'bokapp/books/books_list.html'
    context_object_name = 'books'


class BooksDeleteView(DeleteView):
    model = Books
    pk_url_kwarg = "pk"
    template_name = 'bokapp/books/confirm_delete.html'
    success_url = reverse_lazy('bokapp:book_list')


def bookfilter(request):
    books_list = Books.objects.all()
    books_filter = BooksFilter(request.GET, queryset=books_list)
    return render(request,
                  'bokapp/books/book_filter.html',
                  {'filter': books_filter})


def booklist(request):
    object_list = Books.objects.all()
    books = object_list

    return render(request,
                  'bokapp/books/booklist.html',
                  {'books': books})


def booksearch(request):

    number_of_new_books = 0
    if request.method == 'GET':
        form = BookSearchForm(request.GET)
        if form.is_valid():
            search_result = form.search()

            if len(search_result) < 3 or search_result['totalItems'] == 0 or search_result is False:
                messages.add_message(request, messages.INFO, 'No entry founded')
            else:
                for i in range(len(search_result['items'])):
                    searched_results = (search_result['items'][i]['volumeInfo'])
                    book_verification_list = [keeys for keeys in searched_results.keys()]

                    if "title" in book_verification_list:
                        title = searched_results['title']
                    else:
                        title = "No title"

                    authors = ""
                    try:
                        if len(searched_results['authors']):
                            for k in range(len(searched_results['authors'])):
                                authors += searched_results['authors'][k]
                        else:
                            authors = searched_results['authors'][0]
                    except KeyError:
                        pass

                    isbn_13 = ""
                    for item in searched_results['industryIdentifiers']:
                        if item['type'] == "isbn_13":
                            isbn_13 = item['identifier']
                        elif item['type'] == "OTHER":
                            isbn_13 = item['identifier']

                    if "pageCount" in book_verification_list:
                        pageCount = (searched_results['pageCount'])
                    else:
                        pageCount = 0

                    if "imageLinks" in book_verification_list:
                        imageLinks = (searched_results['imageLinks']['smallThumbnail'])
                    else:
                        imageLinks = "https://via.placeholder.com/150x200"

                    language = (searched_results['language'])

                    try:
                        publishedDate = searched_results['publishedDate'][:4]
                    except KeyError:
                        pass

                    if len(publishedDate) == 4 and len(authors) <= 100:
                        book = Books(
                            title=title,
                            authors=authors,
                            publisheddate=publishedDate,
                            isbn_number=isbn_13,
                            pagecount=pageCount,
                            imagelinks=imageLinks,
                            language=language
                        )
                        book.save()
                        number_of_new_books += 1

            books = Books.objects.all()
            present_books = len(books) - number_of_new_books

            if len(Books.objects.all()[present_books:]) > 0:
                books = Books.objects.all()[present_books:]
                #print("added_books---->>", books)
                return render(request,
                              'bokapp/books/findsearch.html',
                              {'books': books})
    else:
        form = BookSearchForm()

    return render(request,
                  'bokapp/books/book_search.html',
                  {'form': form})



