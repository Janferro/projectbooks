from django.conf.urls import url
from . import viewsapi
from django.urls import path


app_name = 'api'
urlpatterns = [
    path('questions/', viewsapi.BooksAPIView.as_view()),
]