from django.apps import AppConfig


class BokappConfig(AppConfig):
    name = 'bokapp'
