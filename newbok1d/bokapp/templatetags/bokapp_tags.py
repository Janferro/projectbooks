from django import template
from .. models import Books


register = template.Library()


@register.simple_tag
def total_books():
    return Books.objects.count()