from .models import Books
import django_filters



class BooksFilter(django_filters.FilterSet):

    release__joined_year = django_filters.NumberFilter(field_name='publisheddate')
    release_year__gt = django_filters.NumberFilter(field_name='publisheddate',
                                                   lookup_expr='gt')
    release_year__lt = django_filters.NumberFilter(field_name='publisheddate',
                                                   lookup_expr='lt')

    class Meta:
        model = Books
        fields = [
            'title',
            'authors',
            'language'
        ]