# Generated by Django 3.0 on 2019-12-31 14:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bokapp', '0004_auto_20191231_1318'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='books',
            name='slug',
        ),
    ]
