from rest_framework import generics
from..models import Books
from .serializers import BooksSerializer
from rest_framework import filters


class BooksAPIView(generics.ListCreateAPIView):

    search_fields = ['title', 'authors', 'language', 'publishedDate', 'ISBN_number']
    filter_backends = [filters.SearchFilter]
    queryset = Books.objects.all()
    serializer_class = BooksSerializer


